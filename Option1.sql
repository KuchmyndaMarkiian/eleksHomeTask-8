/*1*/
select pName from tProducts where pNumber>0
/*2*/
select pName from tProducts where pName like('%2%')
/*3*/
select pName,pPrice from tProducts where pPrice<10
/*4*/
select pInstance,pName from tProducts where pComment is NULL
/*5*/
select sum(pNumber * pPrice) from tProducts
/*6*/
select left(pName,3) as NamePR,sum(pNumber * pPrice) as SumAll from tProducts group by left(pName,3)
/*7*/
select pName from tProducts where (pInstance not in (select sProduct from tDocDetails where pInstance=sProduct))
/*8*/
select P.pName, count(D.sInstance) from tProducts P,tDocDetails D where P.pInstance=D.sProduct group by P.pName
/*9*/
select p.pName, sum(DD.sNumber) from tProducts P,tDocDetails DD,tDocTypes DT, tDocuments DC
where P.pInstance=DD.sProduct and DD.sDocument=DC.dInstance and DT.tInstance=DC.dType 
and DT.tName like 'Invoice' group by P.pName
/*10*/
select P.pInstance,p.pName, sum(DD.sNumber*P.pPrice) from tProducts P,tDocDetails DD,tDocTypes DT, tDocuments DC
where P.pInstance=DD.sProduct and DD.sDocument=DC.dInstance and DT.tInstance=DC.dType 
and DT.tName like 'Invoice' group by P.pName, P.pInstance order by P.pName



