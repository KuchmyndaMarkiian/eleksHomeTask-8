/*1*/
select cName from tClients
/*2*/
select cName from tClients where cName like 'Cl%'
/*3*/
select cName,cAddress from tClients where not(cAddress is null)
/*4*/
select cInstance,cName from tClients where cAmount<0
/*5*/
select sum(cAmount) from tClients
/*6*/
select cName from tClients where (cInstance not in (select dClient from tDocuments where dClient=cInstance))
/*7*/
select cInstance, cName from tClients where (cInstance in (select dClient from tDocuments where dClient=cInstance)) order by cName desc
/*8*/
select C.cName,count(D.dInstance) as DDocuments from tClients C, tDocuments D where C.cInstance=D.dClient group by C.cName
/*9*/
select C.cName, sum(D.dAmount) from tClients C, tDocuments D, tDocTypes DT
where C.cInstance=D.dClient and D.dType=DT.tInstance and DT.tName like 'Invoice' group by C.cName
/*10*/
select c.cInstance,c.cName, sum(d.dAmount) from tClients c,tDocuments d, tDocTypes dt
where c.cInstance=d.dClient and d.dType= dt.tInstance and (dt.tName not like 'Payment') group by c.cInstance,c.cName order by c.cInstance